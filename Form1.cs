﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Color = System.Drawing.Color;
using Font = System.Drawing.Font;

namespace WindowsFormsApp16
{
    public partial class Form1 : Form
    {
        private DataTable scheduleDataTable;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void LoadStudentScheduleFromFile(string filePath)
        {
            scheduleDataTable = new DataTable();
            scheduleDataTable.Columns.Add("День", typeof(string));
            scheduleDataTable.Columns.Add("Час", typeof(string));
            scheduleDataTable.Columns.Add("Предмет", typeof(string));
            scheduleDataTable.Columns.Add("Викладач", typeof(string));

            try
            {
                using (StreamReader reader = new StreamReader(filePath))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] scheduleEntry = line.Split(',');
                        if (scheduleEntry.Length == 4)
                        {
                            scheduleDataTable.Rows.Add(scheduleEntry);
                        }
                    }
                }

                dataGridView1.DataSource = scheduleDataTable;
            }
            catch (IOException ex)
            {
                MessageBox.Show("Помилка при читанні файлу: " + ex.Message);
            }
        }

        private void завантажитиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Завантажуємо розклад з файлу 
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text Files (*.txt)|*.txt";
            openFileDialog.Title = "Виберіть файл розкладу";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string selectedFilePath = openFileDialog.FileName;
                LoadStudentScheduleFromFile(selectedFilePath);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Видаляє один рядок з датагріду
            if (dataGridView1.SelectedRows.Count > 0)
            {
                for (int i = dataGridView1.SelectedRows.Count - 1; i >= 0; i--)
                {
                    DataGridViewRow row = dataGridView1.SelectedRows[i];
                    if (!row.IsNewRow)
                    {
                        dataGridView1.Rows.Remove(row);
                    }
                }
            }
            else
            {
                MessageBox.Show("Виберіть рядок для видалення.", "Помилка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Додає пустий рядок в датагрід
            DataRow newRow = scheduleDataTable.NewRow();
            scheduleDataTable.Rows.Add(newRow);
        }

        private void зберегтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Зберігаємо датагрід в текстовий файл і якщо стається помилка то спрацьовує catch який містить повідомлення "Помилка при збереженні файлу"
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt";
            saveFileDialog.Title = "Зберегти розклад";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string selectedFilePath = saveFileDialog.FileName;

                try
                {
                    using (StreamWriter writer = new StreamWriter(selectedFilePath))
                    {
                        foreach (DataRow row in scheduleDataTable.Rows)
                        {
                            string scheduleEntry = string.Join(",", row.ItemArray);
                            writer.WriteLine(scheduleEntry);
                        }
                    }

                    MessageBox.Show("Розклад був успішно збережений у файл.", "Інформація",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Помилка при збереженні файлу: " + ex.Message, "Помилка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void зберегтиВЕксельToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Зберігаємо у Excel весь датагрід 
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Excel Files (*.xlsx)|*.xlsx";
            saveFileDialog.Title = "Зберегти у файл Excel";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Create(saveFileDialog.FileName, SpreadsheetDocumentType.Workbook))
                {
                    WorkbookPart workbookPart = spreadsheetDocument.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();
                    SheetData sheetData = new SheetData();
                    WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                    worksheetPart.Worksheet = new Worksheet(sheetData);
                    Sheets sheets = spreadsheetDocument.WorkbookPart.Workbook.AppendChild<Sheets>(new Sheets());
                    Sheet sheet = new Sheet() { Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };
                    sheets.Append(sheet);

                    for (int i = 0; i < dataGridView1.Columns.Count; i++)
                    {
                        Cell cell = new Cell();
                        cell.DataType = CellValues.String;
                        cell.CellValue = new CellValue(dataGridView1.Columns[i].HeaderText);
                        sheetData.AppendChild(new Row(new Cell[] { cell }));
                    }

                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        Row row = new Row();

                        for (int j = 0; j < dataGridView1.Columns.Count; j++)
                        {
                            Cell cell = new Cell();
                            cell.DataType = CellValues.String;
                            cell.CellValue = new CellValue(dataGridView1.Rows[i].Cells[j].Value.ToString());
                            row.AppendChild(cell);
                        }

                        sheetData.AppendChild(row);
                    }
                }

                MessageBox.Show("Дані успішно збережено у файл Excel.", "Інформація", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
        private void SearchBySubject(string searchValue)
        {
            //Функція для пошуку предметів в датагріді за індексом 2 (3 стовпець)
            int columnIndex = 2;

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (row.Cells[columnIndex].Value != null && row.Cells[columnIndex].Value.ToString().Equals(searchValue, StringComparison.OrdinalIgnoreCase))
                {
                    row.DefaultCellStyle.BackColor = Color.Yellow;
                }
                else
                {
                    row.DefaultCellStyle.BackColor = dataGridView1.DefaultCellStyle.BackColor;
                }
            }
        }

        private void зберегтиВЕксельToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            //При натисканні на кнопку викликається функція SearchBySubject яка перевіряє на наявніть зазначеного тексту і підкреслює рядок в жовтий якщо значення в пошуку відповідає значенню в датагріді
            string searchValue = SearchTextBox.Text.Trim();
            SearchBySubject(searchValue);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //При натисканні на кнопку викликається друга форма яка добавляє рядок у основний датагрід і заповнює її значеннями які ввів користувач.
            Form2 addSubjectForm = new Form2();
            if (addSubjectForm.ShowDialog() == DialogResult.OK)
            {
                DataRow newRow = scheduleDataTable.NewRow();
                newRow["День"] = addSubjectForm.Day;
                newRow["Час"] = addSubjectForm.Time;
                newRow["Предмет"] = addSubjectForm.Subject;
                newRow["Викладач"] = addSubjectForm.Teacher;
                scheduleDataTable.Rows.Add(newRow);
            }
        }
    }
}