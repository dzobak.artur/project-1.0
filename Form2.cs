﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp16
{
    public partial class Form2 : Form
    {
        //Створюємо другу форму для того щоб на основний датагрід добавити рядок з цими властивостями.
        public string Day { get; set; }
        public string Time { get; set; }
        public string Subject { get; set; }
        public string Teacher { get; set; }
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Присвоюємо властивості до текстбоксів
            Day = textBox1.Text;
            Time = textBox2.Text;
            Subject = textBox3.Text;
            Teacher = textBox4.Text;
            //Якщо користувач нажимає окей то створюється новий рядок зі значеннями які він ввів і ця форма закривається.
            DialogResult = DialogResult.OK;
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
